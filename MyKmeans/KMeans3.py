import os
import numpy as np
import cv2
from matplotlib import pyplot as plt
# kmeans clustering algorithm
# data = set of data points
# k = number of clusters
# c = initial list of centroids (if provided)
#
def kmeans(data, k, c=None):
    centroids = []

    centroids = [[0, 0, 0], [255, 255, 255]]  

    old_centroids = [[] for i in range(k)] 
    #print old_centroids
    iterations = 0
    while not (has_converged(centroids, old_centroids, iterations)):
        iterations += 1

        clusters = [[] for i in range(k)]

        # assign data points to clusters
        clusters = euclidean_dist(data, centroids, clusters)

        # recalculate centroids
        index = 0
        for cluster in clusters:
            old_centroids[index] = centroids[index]
            #print "print",cluster
            centroids[index] = np.mean(cluster, axis=0).tolist()
            index += 1


#     print("The total number of data instances is: " + str(len(data)))
#     print("The total number of iterations necessary is: " + str(iterations))
#     print("The means of each cluster are: " + str(centroids))
#     print("The clusters are as follows:")
    WhitePix = len(clusters[0])
    BlackPix = len(clusters[1])
    print WhitePix
    print BlackPix
    if WhitePix > 80 and BlackPix > 80:
        print "YAWN DETECETED"
    #print "cluster", clusters
    #for cluster in clusters:
        #print("Cluster with a size of " + str(len(cluster)) + " starts here:")
        #print(np.array(cluster).tolist())
        #print("Cluster ends here.")
        #print "Len", len(cluster)
            
    return

# Calculates euclidean distance between
# a data point and all the available cluster
# centroids.      
def euclidean_dist(data, centroids, clusters):
    #print centroids
    
    for instance in data:  
        # Find which centroid is the closest
        # to the given data point.
        #for i in enumerate(centroids):
            #print i
        mu_index = min([(i[0], np.linalg.norm(instance-centroids[i[0]])) \
                            for i in enumerate(centroids)], key=lambda t:t[1])[0]
        #print mu_index
        try:
            clusters[mu_index].append(instance)
        except KeyError:
            clusters[mu_index] = [instance]

    # If any cluster is empty then assign one point
    # from data set randomly so as to not have empty
    # clusters and 0 means. 
    #print clusters       
    for cluster in clusters:
        #print cluster
        if not cluster:
            cluster.append(data[np.random.randint(0, len(data), size=1)].flatten().tolist())
    print clusters        
    return clusters


# randomize initial centroids
def randomize_centroids(data, centroids, k):
    for cluster in range(0, k):
        centroids.append(data[np.random.randint(0, len(data), size=1)].flatten().tolist())
    #print centroids
    return centroids


# check if clusters have converged    
def has_converged(centroids, old_centroids, iterations):
    MAX_ITERATIONS = 1000
    if iterations > MAX_ITERATIONS:
        return True
    return old_centroids == centroids

data = np.array([[12, 87], [2, 33],  
                 [68, 32], [88, 66], 
                 [79, 89], [1, 12]])
 
poin = np.array([[255, 255, 255],
             [255, 255, 255],
             [255, 255, 255],
             [255, 255, 255],
             [255, 255, 255],
             [255, 255, 255],
             [255, 255, 255],
             [255, 255, 255],
             [255, 255, 255]])


def getImgPixels():
    face_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_frontalface_alt.xml')
    eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye_tree_eyeglasses.xml')
    
    img1 = '../res/face4.PNG'
    img2 ='../res/yawn1.png'
    img = cv2.imread(img2)
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)  # returns Rect(x,y,w,h)
    for (x, y, w, h) in faces: 
        frame = cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
        roi_gray = gray[y:y + h, x:x + w]  # gray face ROI
        roi_color = img[y:y + h, x:x + w]  # color Face ROI
        #print roi_color.shape
        totRows, totColumns = roi_color.shape[:2]
        
        cX = totColumns/3
        rY = totRows/3
        #print totRows, rY
        #print totColumns, cX
        gray_mouth_roi = roi_gray[rY*2:totRows, cX:cX*2]
        color_mouth_roi = roi_color[rY*2:totRows, cX:cX*2]
        #print color_mouth_roi.shape
        #print "X", x, "Y", y, "W", w, "H", h 
        plt.imshow(cv2.cvtColor(color_mouth_roi, cv2.COLOR_RGB2BGR))
        plt.title("Normal Mouth ROI")
        plt.show()
        cv2.waitKey(0)
        #cv2.imshow('Mouth',gray_mouth_roi)
        #resized = imutils.resize(mouth_roi, width=300)
        #ratio = int(mouth_roi.shape[0] / float(resized.shape[0]))
        # convert the resized image to grayscale, blur it slightly,
        # and threshold it
        #gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray_mouth_roi, (5, 5), 0)
        thresh =  cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                cv2.THRESH_BINARY,245,80)
        
        #thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]
        #cv2.imshow('thresh', thresh)
        
        #print 'img1', image.shape
        image = cv2.cvtColor(thresh, cv2.COLOR_GRAY2RGB)
        # reshape the image to be a list of pixels
        #print image.shape
        image = image.reshape((image.shape[0] * image.shape[1], 3))
        
    return image
    
kmeans(poin, 2)