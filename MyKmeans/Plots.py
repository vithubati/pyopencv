import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
style.use('ggplot')

fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)

def animate(i):
    graph_data = open('pix.txt','r').read()
    lines = graph_data.split('\n')
    xs = []
    ys = []
    for line in lines:
        if len(line) > 1:
            x, y = line.split(' ')
            xs.append(x)
            ys.append(y)
    ax1.clear()
    ax1.plot(xs, ys)
    ax1.annotate('Yawning cluster',(xs[330], ys[330]),
                 xytext=(0.8, 0.9), textcoords='axes fraction',
                 arrowprops = dict(facecolor='grey',color='grey'))
    ax1.annotate('Yawning cluster',(xs[500], ys[500]),
                 xytext=(0.8, 0.9), textcoords='axes fraction',
                 arrowprops = dict(facecolor='grey',color='grey'))
    plt.title('Pixels Density during yawning period')
    plt.xlabel('Time interval')
    plt.ylabel('Pixels')
    
ani = animation.FuncAnimation(fig, animate, interval=1000)
plt.show()


# Open a file
# fo = open("data.txt", "r")
# fo2 = open("foo.txt", "w")
# fo3 = open("pix.txt", "wb")
# i=1
# for line in fo:
#     fo3.write(str(i))
#     fo3.write(' ')
#     fo3.write(line)
#     i =i+1
# 
# # Close opend file
# fo.close()