import numpy as np
from matplotlib import pyplot as plt
def calcJ (data, centers):
    
    diffsq = (centers[:, np.newaxis,:] - data)**2    
    return np.sum(np.min(np.sum(diffsq, axis =2), axis =0))

def kmeans ( data , k = 2 , n = 5 ) :
    # I n i t i a l i z e c e n t e r s and l i s t J t o t r a c k per formance m e t r i c
    centers = data[np.random.choice(range(data.shape[0]),k,replace=False),:]
    J = []
    # Repea t n t ime s
    for iteration in range (n) :
    # Which c e n t e r i s each sample c l o s e s t t o ?
    sqdistances = np.sum((centers[:,np.newaxis,:] - data)**2 , axis =2)
    closest = np.argmin(sqdistances, axis =0)
    # C a l c u l a t e J and append t o l i s t J
    J.append(calcJ(data,centers))
    # Update c l u s t e r c e n t e r s
    for i in range(k) :
    centers [ i , : ] = data[ closest == i , :].mean( axis =0)
    # C a l c u l a t e J one f i n a l t ime and r e t u r n r e s u l t s
    J.append (calcJ( data , centers))
    return centers , J , closest

def main():
    x = 2, 4, 5, 9, 10, 12
    y = 3, 8, 11, 15, 16, 17
    d.data = np.vstack((x,y)).T
    d.centers = kmeans(d.data,2,10)
    dclosest = closest(d.centers,d.data)
    plt.close()
    for idx, cen in enumerate(d.closest):
    m = markers [cen];
    c = colors[cen];
    plt.scatter(d.data[idx,0],d.data[idx,1],marker=m,color=c)
    
if __name__ == "__main__": 
    main()