import numpy as np
import cv2
from matplotlib import pyplot as plt
from Queue import Empty
from collections import deque
from Pro.ShapeDetector import ShapeDetector
face_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_frontalface_alt.xml')
eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye_tree_eyeglasses.xml')

fileName = '../res/MyMovie6.mp4'
fileName2 = '../res/MyMovie11.mp4'
s = 1
t = 1
buffer = 32
greenLower = (0, 0, 0)
greenUpper = (32, 32, 32)
pts = deque(maxlen=buffer)
cap = cv2.VideoCapture(fileName)
fps = cap.get(cv2.CAP_PROP_FPS)
print "Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps)

      
print 'System Start'
while(1):
    cv2.waitKey(1)
    
    if cap.isOpened():
        #print 'captured'
        # Take each frame
        captured, frame = cap.read()
        
        if captured:
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            faces = face_cascade.detectMultiScale(gray, 1.3, 5)  # returns Rect(x,y,w,h)
            if len(faces) >0:
                #print 'face len', len(faces)             
                for (x, y, w, h) in faces: 
                    frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
                    roi_gray = gray[y:y + h, x:x + w]  # gray face ROI
                    roi_color = frame[y:y + h, x:x + w]  # color Face ROI
                    eyes = eye_cascade.detectMultiScale(roi_gray)
                    
                    if len(eyes) >1:                    
                        #print 'len', len(eyes) 
                        for (ex, ey, ew, eh) in eyes:  # returns Eye Region
                            #print "x y , h w", ex, ey, ew, eh
                            #print 'eye Found'
                            roi_color_eye = roi_color[ey:ey + eh, ex:ex + ew]
                            #plt.imshow(cv2.cvtColor(roi_gray_eye, cv2.COLOR_BGR2RGB))
                            #plt.show()
                            roi_gray_eye = cv2.cvtColor(roi_color_eye, cv2.COLOR_BGR2GRAY)  # gray Eye ROI
                            #cv2.imshow("Color", roi_color_eye)
                            #cv2.imshow("Color", roi_gray_eye) 
                            #print roi_gray_eye.shape
                            # we need to keep in mind aspect ratio so the image does
                            # not look skewed or distorted -- therefore, we calculate
                            # the ratio of the new image to the old image
                            r = 100.0 / roi_gray_eye.shape[1]
                            dim = (100, int(roi_gray_eye.shape[0] * r))
                            resized_roi_gray_eye =  cv2.resize(roi_gray_eye, (100,100))
                            resized_roi_color_eye =  cv2.resize(roi_color_eye, (100,100))
                            #cv2.imshow("Sssssssssss", res)
                            # perform the actual resizing of the image and show it
                            #resized_roi_gray_eye = cv2.resize(roi_gray_eye, dim, interpolation = cv2.INTER_AREA)
                            #resized_roi_color_eye = cv2.resize(roi_color_eye, dim, interpolation = cv2.INTER_AREA)
                            #cv2.imshow("Sssssssdfssss", resized_roi_gray_eye)
                            #cv2.waitKey(0)
                            #print "resized", resized_roi_gray_eye.shape
                            #---- Contour Part
                            blurred = cv2.GaussianBlur(resized_roi_gray_eye, (11, 11), 0)
                            thresh =  cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                                    cv2.THRESH_BINARY,105,40) #50 to 30
                            bgr = cv2.cvtColor(thresh, cv2.COLOR_GRAY2BGR)
                            hsv = cv2.cvtColor(bgr, cv2.COLOR_BGR2HSV)
                            
                            #cv2.imshow("vithu", hsv)
                            # construct a mask for the color "green", then perform
                            # a series of dilations and erosions to remove any small
                            # blobs left in the mask
                            mask = cv2.inRange(hsv, greenLower, greenUpper)
                            #print np.unique(mask.tolist())
                            mask = cv2.erode(mask, None, iterations=2)
                            mask = cv2.dilate(mask, None, iterations=2)
                            # find contours in the mask and initialize the current
                            # (x, y) center of the ball
                            cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_SIMPLE)[-2]
                            center = None
                            # only proceed if at least one contour was found
                            #print "contours Len", len(cnts)
                            ratio = int(frame.shape[0] / float(frame.shape[0]))
                            sd = ShapeDetector()
                            #print "contours Len", len(cnts)
                            if len(cnts) > 0:
                                # find the largest contour in the mask, then use
                                # it to compute the minimum enclosing circle and
                                # centroid
                                area = cv2.contourArea(cnts[0])
                                #print "Area", area
                                c = max(cnts, key=cv2.contourArea)
                                M = cv2.moments(c)
                                cX = int((M["m10"] / M["m00"]) * ratio)
                                cY = int((M["m01"] / M["m00"]) * ratio)
                                shape = sd.detect(c)
                                
                                if shape == 4:
                                    print "Shape: ",shape
                                    ((x, y), radius) = cv2.minEnclosingCircle(c)
                                    M = cv2.moments(c)
                                    center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
                                    #print "Radius", radius
                                    # only proceed if the radius meets a minimum size
                                    if (radius > 0) and (radius <20):
                                        # draw the circle and centroid on the frame,
                                        # then update the list of tracked points
                                        #cv2.circle(roi_color, (int(x), int(y)), int(radius),
                                         #   (0, 255, 255), 2)
                                        cv2.circle(resized_roi_color_eye, center, 5, (0, 0, 255), -1)
                            plt.imshow(cv2.cvtColor(resized_roi_color_eye, cv2.COLOR_BGR2RGB))
                            plt.show()
                            cv2.waitKey(0)
                            # update the points queue
#                             pts.appendleft(center)
#                             # loop over the set of tracked points
#                             for i in xrange(1, len(pts)):
#                                 # if either of the tracked points are None, ignore
#                                 # them
#                                 if pts[i - 1] is None or pts[i] is None:
#                                     continue
#                             
#                                 # otherwise, compute the thickness of the line and
#                                 # draw the connecting lines
#                                 thickness = int(np.sqrt(buffer / float(i + 1)) * 2.5)
#                                 cv2.line(resized_roi_color_eye, pts[i - 1], pts[i], (0, 0, 255), thickness)
#                             
                            # show the frame to our screen
                            
                            default_size = cv2.resize(resized_roi_color_eye, roi_gray_eye.shape)
                            #print "Default shape",default_size.shape
                            #cv2.imshow("asas", default_size)
                            totRows, totColumns = default_size.shape[:2]
                            roi_color[ey:ey+totRows, ex:ex+totColumns] = default_size
                            #cv2.imshow("Frame", frame)
                            #cv2.waitKey(0)
                            key = cv2.waitKey(1) & 0xFF
                          
                            # if the 'q' key is pressed, stop the loop
                            if key == ord("q"):
                                break

                            #---- Contour Part End
                    else:
                        print 'No eyes'
            else:
                print 'no face' 
        else:
            print 'no frame captured'
            break
        #=======================================================================
        # plt.subplot(121), plt.imshow(color_eye_roi, cmap='gray')
        # plt.title('Found circle Image'), plt.xticks([]), plt.yticks([])
        # plt.subplot(122), plt.imshow(img, cmap='gray')
        # plt.title('original Image'), plt.xticks([]), plt.yticks([])            
        # plt.show() 
        #=======================================================================
        #cv2.imshow('vithu', color_eye_roi)
        #if cv2.waitKey(5) & 0xFF == ord('q'):
            #break
        #print 'Number of Frame', s
        #s=s+1
    else:
        print 'no video opened'   
cap.release()             
#cv2.imshow('vithu', img)
#cv2.waitKey(0)
   
