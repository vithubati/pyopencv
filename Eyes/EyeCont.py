import cv2
import numpy as np
import imutils
img = cv2.imread("../res/eye3.PNG",0)
blurred = cv2.GaussianBlur(img, (5, 5), 0)
thresh =  cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
        cv2.THRESH_BINARY,105,50)

#thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]
cv2.imshow('thresh', thresh)
tarr = np.asarray(thresh)
print 'Thresh', thresh
#cv2.imshow('ssas',thresh[30,40])

# find contours in the thresholded image
# cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
# cnts = cnts[0] if imutils.is_cv2() else cnts[1]
# print "Tot Counts", len(cnts)
# for c in cnts:
#     print "c Shape", c.shape[:2]
#     peri = cv2.arcLength(c, True)
#     print "Peri", peri
#     approx = cv2.approxPolyDP(c, 0.04 * peri, True)
#     print "approx", approx.shape
#     print len(approx)
#  
#     cv2.drawContours(img, [c], -1, (0, 255, 0), 1)
#     print "Shape", img.shape 
#     cv2.imshow('dd',img)
#     cv2.waitKey(0)
# 
# cv2.imshow('dd',img)
# cv2.waitKey(0)

image = cv2.imread("../res/thresh.PNG")
output = image.copy()
#gray = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
grays = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow('dd',grays)
print thresh.shape
print grays.shape
# detect circles in the image
circles = cv2.HoughCircles(grays, cv2.HOUGH_GRADIENT, 2, 10,param1=50, param2=30, minRadius=1, maxRadius=100) 
# ensure at least some circles were found
if circles is not None:
    # convert the (x, y) coordinates and radius of the circles to integers
    #circles = np.round(circles[0, :]).astype("int")
 
    circles = np.uint16(np.around(circles))           
    for i in circles[0,:]:
        print 'Deteccted iris', i
        # draw the outer circle
        cv2.circle(output, (i[0], i[1]), i[2], (0, 255, 0),1)
        # draw the center of the circle
        #cv2.circle(output, (i[0], i[1]), 2, (0, 0, 255),1)            
 
    # show the output image
    cv2.imshow("output", np.hstack([image, output]))
    cv2.waitKey(0)
else:
    print "No circles"
cv2.waitKey(0)
