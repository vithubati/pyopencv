import cv2
import numpy as np
import imutils
from collections import deque

buffer = 32
greenLower = (0, 0, 0)
greenUpper = (32, 32, 32)
pts = deque(maxlen=buffer)

img = cv2.imread("../res/awesome.PNG")
gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(gray_img, (11, 11), 0)
thresh =  cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
        cv2.THRESH_BINARY,105,40) #50 to 30
bgr = cv2.cvtColor(thresh, cv2.COLOR_GRAY2BGR)
hsv = cv2.cvtColor(bgr, cv2.COLOR_BGR2HSV)

cv2.imshow("vithu", bgr)
# construct a mask for the color "green", then perform
# a series of dilations and erosions to remove any small
# blobs left in the mask
mask = cv2.inRange(hsv, greenLower, greenUpper)
print np.unique(mask.tolist())
mask = cv2.erode(mask, None, iterations=2)
mask = cv2.dilate(mask, None, iterations=2)
# find contours in the mask and initialize the current
# (x, y) center of the ball
cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_SIMPLE)[-2]
center = None
# only proceed if at least one contour was found
print "contours Len", len(cnts)
if len(cnts) > 0:
    # find the largest contour in the mask, then use
    # it to compute the minimum enclosing circle and
    # centroid
    area = cv2.contourArea(cnts[0])
    print "Area", area
    c = max(cnts, key=cv2.contourArea)
    ((x, y), radius) = cv2.minEnclosingCircle(c)
    M = cv2.moments(c)
    center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
    print "Radius", radius
    # only proceed if the radius meets a minimum size
    if (radius > 0) and (radius <20):
        # draw the circle and centroid on the frame,
        # then update the list of tracked points
        cv2.circle(img, (int(x), int(y)), int(radius),
            (0, 255, 255), 2)
        cv2.circle(img, center, 5, (0, 0, 255), -1)

# update the points queue
pts.appendleft(center)
# loop over the set of tracked points
for i in xrange(1, len(pts)):
    # if either of the tracked points are None, ignore
    # them
    if pts[i - 1] is None or pts[i] is None:
        continue

    # otherwise, compute the thickness of the line and
    # draw the connecting lines
    thickness = int(np.sqrt(buffer / float(i + 1)) * 2.5)
    cv2.line(img, pts[i - 1], pts[i], (0, 0, 255), thickness)

# show the frame to our screen
cv2.imshow("Frame", img)
cv2.waitKey(0)
#     key = cv2.waitKey(1) & 0xFF
#  
#     # if the 'q' key is pressed, stop the loop
#     if key == ord("q"):
#         break

# cleanup the camera and close any open windows
#camera.release()
cv2.destroyAllWindows()
