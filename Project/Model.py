# import the necessary packages
import numpy as np
import cv2
class Model(object): 
    def __init__(self):
        pass
        
    def yawnDetector(self, clt):
        # grab the number of different clusters and create a histogram
        # based on the number of pixels assigned to each cluster
        detected = False
        try:
            numLabels = np.arange(0, len(np.unique(clt.labels_)))
            (hist, _) = np.histogram(clt.labels_, bins=numLabels)
            # print len(clt.labels_)
            labels = clt.labels_
            labelsList = labels.tolist()
            # print "white Pixels:", labelsList.count(0)
            # print "Black Pixels:", labelsList.count(1)
            if labelsList.count(0) > 80 and labelsList.count(1) > 80:
                detected = True           
            # print (clt.labels_).count(0)
            # normalize the histogram, such that it sums to one
        except:
            detected = False
        finally:
            return detected
    
    def plot_colors(self, hist, centroids):
        try:
            # initialize the bar chart representing the relative frequency
            # of each of the colors
            bar = np.zeros((50, 300, 3), dtype="uint8")
            startX = 0
         
            # loop over the percentage of each cluster and the color of
            # each cluster
            for (percent, color) in zip(hist, centroids):
                # plot the relative percentage of each cluster
                endX = startX + (percent * 300)
                cv2.rectangle(bar, (int(startX), 0), (int(endX), 50),
                    color.astype("uint8").tolist(), -1)
                startX = endX
        except:
            pass
        finally:    
            # return the bar chart
            return bar

    def detectCircle(self, c):
        length = 0
        try:
            # calculating the Perimeter of the contour
            peri = cv2.arcLength(c, True)
            # finding the angles of the contour with perimeter
            approx = cv2.approxPolyDP(c, 0.1 * peri, True)
            length = len(approx)
        except:
            pass
        finally:
            return length