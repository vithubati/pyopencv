from sklearn.cluster import KMeans
import cv2
from Project import Model

class YawnDetection:
    def __init__(self):
        pass
    
    def detectYawning(self, roi_gray):
        detected = False
        try:
            totRows, totColumns = roi_gray.shape[:2]      
            cX = totColumns/3
            rY = totRows/3
            #print totRows, rY
            #print totColumns, cX
            gray_mouth_roi = roi_gray[rY*2:totRows, cX:cX*2]
            #print color_mouth_roi.shape
            #print "X", x, "Y", y, "W", w, "H", h 
            
            blurred = cv2.GaussianBlur(gray_mouth_roi, (5, 5), 0)
            thresh =  cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                    cv2.THRESH_BINARY_INV,245,80)
            
            image = cv2.cvtColor(thresh, cv2.COLOR_GRAY2RGB)
            #reshape the image to be a list of pixels
            #print image.shape
            image = image.reshape((image.shape[0] * image.shape[1], 3))
            #print image
            #print 'img2', len(image)
            #print image[1700:1900]
            # cluster the pixel intensities
            kMeanclt = KMeans(n_clusters = 2)
            kMeanclt.fit(image)
            
            centorids = kMeanclt.cluster_centers_
            labels = kMeanclt.labels_
            #print len(labels)
            #print kMeanclt.
            colors = ["g.", "r."]
            
            #Model obj creation
            detector = Model()
            # build a histogram of clusters and then create a figure
            # representing the number of pixels labeled to each color
            detected = detector.yawnDetector(kMeanclt)
        except:
            detected = False                       
        finally:
            return detected
        