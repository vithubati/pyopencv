import cv2
from Project import Model
from Project import KMeans
class YawnDetection2:
    def __init__(self):
        pass
    
    def detectYawning(self, roi_gray):
        detected = False
        c = 30
        YAWN_THRESHOLD = 12
        try:
            totRows, totColumns = roi_gray.shape[:2]      
            cX = totColumns/3
            rY = totRows/3
            gray_mouth_roi = roi_gray[rY*2:totRows, cX:cX*2]
            
            blurred = cv2.GaussianBlur(gray_mouth_roi, (5, 5), 0)
            if blurred.shape[0] < 40:
                c =  80
                YAWN_THRESHOLD = 7
            thresh =  cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                    cv2.THRESH_BINARY_INV,245,c)
            # cv2.THRESH_BINARY_INV,245,80
            # cv2.THRESH_BINARY_INV,71,30)
            image = cv2.cvtColor(thresh, cv2.COLOR_GRAY2RGB)
            #reshape the image to be a list of pixels
            #print image.shape
            cv2.imshow("ROI", gray_mouth_roi)
            cv2.imshow("ROI2", image)
            image = image.reshape((image.shape[0] * image.shape[1], 3))
            
            brightArea, darkArea = KMeans.kmeans(pixels = image, k_clusters = 2)
            
            brightPercent = round(float(brightArea)/ (float(brightArea) + float(darkArea)) * 100)
            darkPercent = round(float(darkArea) / (float(brightArea) + float(darkArea)) * 100)
            #print brightPercent, darkPercent
            if brightPercent > YAWN_THRESHOLD and darkPercent > YAWN_THRESHOLD:
                detected = True  
          
        except:
            detected = False                       
        finally:
            return detected
        