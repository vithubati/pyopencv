from matplotlib import pyplot as plt
from Queue import Empty
from collections import deque
import numpy as np
import cv2
from Queue import Empty
from Project import Model
eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye_tree_eyeglasses.xml')
class IrisDetectionB:
    
    buffer = 32
    blackLower = (0, 0, 0)
    blackUpper = (32, 32, 32)
    def __init__(self):
        pass
    
    def detectIris(self, roi_color, roi_gray):
        eyes = eye_cascade.detectMultiScale(roi_gray)
        iDetected = False
        try:          
            if len(eyes) >1:           
                for (ex, ey, ew, eh) in eyes:  # returns Eye Region
                    #print "x y , h w", ex, ey, ew, eh
                    #print 'eye Found'
                    roi_color_eye = roi_color[ey:ey + eh, ex:ex + ew]
                    #plt.imshow(cv2.cvtColor(roi_gray_eye, cv2.COLOR_BGR2RGB))
                    #plt.show()
                    roi_gray_eye = cv2.cvtColor(roi_color_eye, cv2.COLOR_BGR2GRAY)  # gray Eye ROI
                    #cv2.imshow("Color", roi_color_eye)
                    #cv2.imshow("Color", roi_gray_eye) 
                    #print roi_gray_eye.shape
                    # we need to keep in mind aspect ratio so the image does
                    # not look skewed or distorted -- therefore, we calculate
                    # the ratio of the new image to the old image
                    r = 100.0 / roi_gray_eye.shape[1]
                    dim = (100, int(roi_gray_eye.shape[0] * r))
                    resized_roi_gray_eye =  cv2.resize(roi_gray_eye, (100,100))
                    #resized_roi_color_eye =  cv2.resize(roi_color_eye, (100,100))
                    #cv2.imshow("Sssssssssss", res)
                    # perform the actual resizing of the image and show it
                    #resized_roi_gray_eye = cv2.resize(roi_gray_eye, dim, interpolation = cv2.INTER_AREA)
                    #resized_roi_color_eye = cv2.resize(roi_color_eye, dim, interpolation = cv2.INTER_AREA)
                    #cv2.normalize(resized_roi_gray_eye,resized_roi_gray_eye,0,160,cv2.NORM_MINMAX)
                    #cv2.imshow("Sssssssdfssss", resized_roi_gray_eye)
                    #cv2.waitKey(0)
                    #print "resized", resized_roi_gray_eye.shape
                    #---- Contour Part
                    #ret,thresh = cv2.threshold(resized_roi_gray_eye,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
                    #print ret
                    blurred = cv2.GaussianBlur(resized_roi_gray_eye, (11, 11), 0)
                    #cv2.imshow("vithu", blurred)
                    #cv2.waitKey(0)
                    thresh =  cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                            cv2.THRESH_BINARY,105,40) #50 to 30 #105,40 #245,32
                    bgr = cv2.cvtColor(thresh, cv2.COLOR_GRAY2BGR)
                    hsv = cv2.cvtColor(bgr, cv2.COLOR_BGR2HSV)
                    
                    #cv2.imshow("vithu", hsv)
                    # construct a mask for the color "black", then perform
                    # a series of dilations and erosions to remove any small
                    # blobs left in the mask
                    mask = cv2.inRange(hsv, self.blackLower, self.blackUpper)
                    #print np.unique(mask.tolist())
                    mask = cv2.erode(mask, None, iterations=2)
                    mask = cv2.dilate(mask, None, iterations=2)
                    # find contours in the mask and initialize the current
                    # (x, y) center of the ball
                    #cv2.normalize(mask,mask,0,160,cv2.NORM_MINMAX)
                    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)[-2]
                    center = None
                    # only proceed if at least one contour was found
                    #print "contours Len", len(cnts)
                    model = Model()
                    #print "Contours", len(cnts)
                    if len(cnts) > 0:
                        # find the largest contour in the mask, then use
                        # it to compute the minimum enclosing circle and
                        # centroid
                        area = cv2.contourArea(cnts[0])
                        #print "Area", area
                        c = max(cnts, key=cv2.contourArea)
                        M = cv2.moments(c)
                        shape = model.detectCircle(c)  
                        #print "Shape: ",shape                      
                        if shape == 4:
                            #print "Shape: ",shape
                            ((x, y), radius) = cv2.minEnclosingCircle(c)
                            #M = cv2.moments(c)
                            #center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
                            #print "Radius", radius
                            # only proceed if the radius meets a minimum size
                            if (radius > 0) and (radius <20):
                                iDetected = True
                                break
                            else:
                                iDetected = False                                                                                       
                        else:
                            iDetected = False      
                    else:
                        #print "No Iris"
                        iDetected = False  
                        break     
            else:
                iDetected = False
        except:
            print "IrisDetectionException"
            iDetected = False        
        finally:
            return iDetected    


