import cv2
import numpy as np

#BGR format
blue  = np.uint8([[[255,0,0]]])
green = np.uint8([[[0,255,0]]])
red   = np.uint8([[[0,0,255]]])
black = np.uint8([[[0,0,0]]])
hsv_green = cv2.cvtColor(green,cv2.COLOR_BGR2HSV)
hsv_blue = cv2.cvtColor(blue,cv2.COLOR_BGR2HSV)
hsv_red = cv2.cvtColor(red,cv2.COLOR_BGR2HSV)
hsv_black = cv2.cvtColor(black,cv2.COLOR_BGR2HSV)

print 'Blue  ' ,hsv_blue
print 'Green ', hsv_green
print 'Red   ', hsv_red
print 'Black ', hsv_black

#===============================================================================
# Blue   [[[120 255 255]]]
# Green  [[[ 60 255 255]]]
# Red    [[[  0 255 255]]]
#===============================================================================
