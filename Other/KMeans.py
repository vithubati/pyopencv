# import the necessary packages

from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import argparse
import utils
import cv2
 

image = cv2.imread('../res/bl.png',0)

image =  cv2.adaptiveThreshold(image,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv2.THRESH_BINARY,245,80)

#print 'img1', len(image)
#print 'img1', image.shape
image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
# reshape the image to be a list of pixels
print image.shape
image = image.reshape((image.shape[0] * image.shape[1], 3))
#print image
#print 'img2', len(image)
#print image[1700:1900]
# cluster the pixel intensities
kMeanclt = KMeans(n_clusters = 2)
kMeanclt.fit(image)

centorids = kMeanclt.cluster_centers_
labels = kMeanclt.labels_
print len(labels)
#print kMeanclt.
colors = ["g.", "r."]
# for i in range(len(image)):
#     #print ("Colour Values:", image[i], "Labels:", labels[i])
#     plt.plot(image[i][0], image[0][1], colors[labels[i]], markersize=10)
# 
# plt.scatter(centorids[:,0], centorids[:,1], marker="x", s=150, linewidths=5, zorder= 10, label = ["Vithu", "hh"])
#     
for i in range(len(image)):
    #print ("Colour Values:", image[i], "Labels:", labels[i])
    plt.plot(image[i][0], image[0][1], colors[labels[i]], markersize=10)

plt.scatter(centorids[:,0], centorids[:,1], marker="x", s=150, linewidths=5, zorder= 10, label = ["Vithu", "hh"])
plt.title('K-means\'s clusters')
plt.xlabel('Region\'s Pixels')
plt.ylabel('Region\'s Pixels')    

# build a histogram of clusters and then create a figure
# representing the number of pixels labeled to each color
hist = utils.centroid_histogram(kMeanclt)
bar = utils.plot_colors(hist, kMeanclt.cluster_centers_)

font_dict = {'family':'serif',
                 'color':'darkred',
                 'size':15}
x=[2,4,210,8]
y=[4, 8, 60]
# show our image
plt.figure()
plt.axis("off")
plt.title('K-means\'s clustered Regions')
plt.text(x[3], y[2],'Mouth region', fontdict=font_dict)
plt.text(x[2], y[2],'Yawning region', fontdict=font_dict)
plt.imshow(bar)
plt.show()

