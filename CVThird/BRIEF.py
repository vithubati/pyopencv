import numpy as np
import cv2
from matplotlib import pyplot as plt

img = cv2.imread('../src/buterflygray.png',0)

# Initiate STAR detector
star = cv2.FeatureDetector_create("STAR") #these methods are nonefree in 3.0

# Initiate BRIEF extractor
brief = cv2.DescriptorExtractor_create("BRIEF")

# find the keypoints with STAR
kp = star.detect(img,None)

# compute the descriptors with BRIEF
kp, des = brief.compute(img, kp)

print brief.getInt('bytes')
print des.shape
cv2.imshow('BRIEF',des)

cv2.waitKey(0)