import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('../res/eye3.PNG',0)
img2 = cv2.imread('../res/eye3.PNG',0)
#img = cv2.medianBlur(img,5)
ret,thresh1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
# bluring the moice which will give good output 
blur = cv2.GaussianBlur(img,(5,5),0)
edges = cv2.Canny(blur,100,200)


#drowing circle on the circle edge
cimg = cv2.cvtColor(edges,cv2.COLOR_GRAY2BGR)
circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,20,
                            param1=50,param2=30,minRadius=0,maxRadius=0)

circles = np.uint16(np.around(circles))
for i in circles[0,:]:
    # draw the outer circle
    cv2.circle(img2,(i[0],i[1]),i[2],(0,255,0),2)
    # draw the center of the circle
    cv2.circle(img2,(i[0],i[1]),2,(0,0,255),3)

cvt = cv2.cvtColor(img2, cv2.COLOR_GRAY2BGR)    
cv2.imshow('vi', cvt)
cv2.waitKey(0)

plt.subplot(121),plt.imshow(edges,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(img2,cmap = 'gray')
plt.title('Found circle Image'), plt.xticks([]), plt.yticks([])

plt.show()