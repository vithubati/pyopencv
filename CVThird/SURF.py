import numpy as np
import cv2
from matplotlib import pyplot as plt

imge = cv2.imread('../res/buterfly.jpg')
img = cv2.cvtColor(imge, cv2.COLOR_RGB2GRAY)
# Create SURF object. You can specify params here or later.
# Here I set Hessian Threshold to 400
surf = cv2.xfeatures2d.SURF_create(400)

# Find keypoints and descriptors directly
kp, des = surf.detectAndCompute(img,None)
len(kp)
# Check present Hessian threshold
print surf.hessianThreshold

# We set it to some 50000. Remember, it is just for representing in picture.
# In actual cases, it is better to have a value 300-500
surf.hessianThreshold = 50000

# Again compute keypoints and check its number.
kp, des = surf.detectAndCompute(img,None)

print len(kp)
img2 = cv2.drawKeypoints(img,kp,None,(255,0,0),4)
#plt.imshow(img2),plt.show()


# Check upright flag, if it False, set it to True
print surf.upright
surf.upright = True
# Recompute the feature points and draw it
kp = surf.detect(img,None)
img2 = cv2.drawKeypoints(img,kp,None,(255,0,0),4)

# Find size of descriptor
print surf.descriptorSize()

# That means flag, "extended" is False.
surf.extended

# So we make it to True to get 128-dim descriptors.
surf.extended = True
kp, des = surf.detectAndCompute(img,None)
print surf.descriptorSize()
#print des.shape
plt.imshow(img2),plt.show()

