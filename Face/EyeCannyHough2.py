import numpy as np
import cv2
from matplotlib import pyplot as plt
from cv2 import threshold, grabCut
face_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_frontalface_alt.xml')
eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye_tree_eyeglasses.xml')

img = cv2.imread('../res/face7.PNG')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

faces = face_cascade.detectMultiScale(gray, 1.3, 5)  # returns Rect(x,y,w,h)
for (x, y, w, h) in faces: 
    frame = cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
    roi_gray = gray[y:y + h, x:x + w]  # gray face ROI
    roi_color = img[y:y + h, x:x + w]  # color Face ROI
    eyes = eye_cascade.detectMultiScale(roi_gray)
    print 'Eye Roi: ', eyes
    #cv2.imshow('vv',img)
    #cv2.waitKey(0) 
    for (ex, ey, ew, eh) in eyes:  # returns Eye Region
        # cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2) 
        # cv2.rectangle(roi_color,(ex,ey+10),(ex+ew,ey+eh-10),(0,255,0),2)
        #=======================================================================
        # roi_gray_eye = ~roi_color[ey:ey + eh, ex:ex + ew]
        # cv2.imshow('image2', roi_gray_eye)
        # roi_gray_eye = cv2.cvtColor(roi_gray_eye, cv2.COLOR_BGR2GRAY)  # gray Eye ROI
        # 
        #=======================================================================
        color_eye_roi = roi_color[ey:ey + eh, ex:ex + ew]
        #cv2.imshow('image3', roi_color_eye)  # color face ROI
        # blur = cv2.GaussianBlur(roi_eye,(5,5),0) 
        #cv2.waitKey(0)  
        # img = cv2.medianBlur(img,5)
        #=======================================================================
        # ret, thresh1 = cv2.threshold(roi_color_eye, 127, 255, cv2.THRESH_BINARY)
        # # bluring the moice which will give good output 
        # roi_color_eye = cv2.cvtColor(roi_color_eye, cv2.COLOR_BGR2GRAY)
        # blur = cv2.GaussianBlur(roi_color_eye, (5, 5), 0)
        # edges = cv2.Canny(blur, 100, 200)
        # 
        # cv2.imshow('edgee', edges);
        # cv2.waitKey(0)
        #=======================================================================
        # drawing circle on the circle edge
        
        gray_eye_roi = cv2.cvtColor(color_eye_roi, cv2.COLOR_BGR2GRAY)
        #plt.title('Grayscale Image')
        #plt.imshow(cv2.cvtColor(gray_eye_roi, cv2.COLOR_GRAY2RGB))
        #plt.show()
        
        #ret, thrsh_eye_roi = cv2.threshold(gray_eye_roi, 120, 250, cv2.THRESH_BINARY) 
        #plt.title('thresh Image')
        #plt.imshow(cv2.cvtColor(thrsh_eye_roi, cv2.COLOR_GRAY2RGB))
        #plt.show()
        
        #gray_eye_roi = cv2.equalizeHist(gray_eye_roi)
        #cv2.imshow('vithu7', gray_eye_roi)
        cv2.waitKey(0)
        
        #cv2.cvtColor(gray_eye_roi, cv2.COLOR_GRAY2BGR)
                
        circles = cv2.HoughCircles(gray_eye_roi, cv2.HOUGH_GRADIENT, 1, 300,
                                    param1=50, param2=30, minRadius=0, maxRadius=0)  
        print gray_eye_roi.dtype                    
        if circles is not None:             
            circles = np.uint16(np.around(circles))           
            for i in circles[0,:]:
                print 'Deteccted iris', i
                # draw the outer circle
                cv2.circle(color_eye_roi, (i[0], i[1]), i[2], (0, 255, 0),1)
                # draw the center of the circle
                cv2.circle(color_eye_roi, (i[0], i[1]), 2, (0, 0, 255),1)                               
                cv2.imshow('eyes', cv2.cvtColor(gray_eye_roi,cv2.COLOR_GRAY2BGR))
                cv2.waitKey(0)
                plt.subplot(121), plt.imshow(cv2.cvtColor(color_eye_roi, cv2.COLOR_BGR2RGB), cmap='gray')
                plt.title('Found Iris'), plt.xticks([]), plt.yticks([])
                plt.subplot(122), plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB), cmap='gray')
                plt.title('original Image'), plt.xticks([]), plt.yticks([])            
                plt.show() 
                break
        else:
            print 'No iris found'   
#cv2.imshow('vithu', img)
#cv2.waitKey(0)
   
