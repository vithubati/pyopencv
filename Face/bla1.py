import numpy as np
import cv2
from matplotlib import pyplot as plt

face_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_frontalface_alt.xml')
eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye.xml')

img = cv2.imread('../res/face8.PNG')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# Setup SimpleBlobDetector parameters.
params = cv2.SimpleBlobDetector_Params()
# Change thresholds
params.minThreshold = 10
params.maxThreshold = 200
# Filter by Area.
params.filterByArea = True
params.minArea = 10

faces = face_cascade.detectMultiScale(gray, 1.3, 5) # returns Rect(x,y,w,h)
for (x,y,w,h) in faces: 
    img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
    roi_gray = gray[y:y+h/2, x:x+w]
    roi_color = img[y:y+h, x:x+w]
    eyes = eye_cascade.detectMultiScale(roi_gray)
    for (ex,ey,ew,eh) in eyes:
        cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
        #cv2.rectangle(roi_color,(ex,ey+10),(ex+ew,ey+eh-10),(0,255,0),2)
        ver = (cv2.__version__).split('.')
        if int(ver[0]) < 3 :
            detector = cv2.SimpleBlobDetector_Params(params)
        else : 
            detector = cv2.SimpleBlobDetector_create(params)
        roi_gray_eye = roi_gray[ey:ey+eh, ex:ex+ew]
        roi_color_eye = roi_color[ey:ey+eh, ex:ex+ew]   
        #cv2.imshow('g', roi_gray_eye)
        # Detect blobs.
        keypoints = detector.detect(roi_gray_eye)
        for sp in keypoints:
            color = (0, 0, 255)
            x, y = sp.pt
            cv2.circle(roi_color_eye, (int(x), int(y)), 5, color)
        #im_with_keypoints = cv2.drawKeypoints(roi_color_eye, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

#cv2.namedWindow('image', cv2.WINDOW_NORMAL)
b,g,r = cv2.split(img)       # get b,g,r
rgb_img = cv2.merge([r,g,b]) 
plt.imshow(rgb_img,'gray'),plt.show()
cv2.waitKey(0)
cv2.destroyAllWindows()