import numpy as np
import cv2
from matplotlib import pyplot as plt
face_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_frontalface_alt.xml')
eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye_tree_eyeglasses.xml')

img = cv2.imread('../res/face2.PNG')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

faces = face_cascade.detectMultiScale(gray, 1.3, 5) # returns Rect(x,y,w,h)
for (x,y,w,h) in faces: 
    img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
    roi_gray = gray[y:y+h, x:x+w]
    roi_color = img[y:y+h, x:x+w]
    eyes = eye_cascade.detectMultiScale(roi_gray)
    for (ex,ey,ew,eh) in eyes:
        #returns Eye Region
        #cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2) 
        cv2.rectangle(roi_color,(ex,ey+20),(ex+ew,ey+eh-20),(0,255,0),2)
         
        roi_gray_eye = ~roi_color[ey:ey+eh, ex:ex+ew]
        roi_gray_eye = cv2.cvtColor(roi_gray_eye, cv2.COLOR_BGR2GRAY)
        roi_color_eye = roi_color[ey:ey+eh-20, ex:ex+ew]
        #blur = cv2.GaussianBlur(roi_eye,(5,5),0)     
        cv2.imshow("vvvvvvvvvvvv", roi_color_eye) 
        ret,thresh = cv2.threshold(roi_gray_eye,140,255,cv2.THRESH_BINARY) # cv2.THRESH_BINARY+cv2.THRESH_OTSU
         
        # Find all contours
        #cv2.imshow('gray1',roi_gray_eye),plt.show()
        cv2.waitKey(0)
        image, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
        print len(contours)
        # Fill all spaces in each contour with white color by passing value of -1 to line thickness
        # i gave -1 for the third arg to draw on all contours
        
        #### Get only pupil then apply that in real image
        cv2.drawContours(roi_gray_eye, contours, -1, (255,255,255), -1)
        #ss = cv2.cvtColor(~roi_color_eye,cv2.COLOR_BGR2GRAY)
        cv2.cvtColor(roi_gray_eye,cv2.COLOR_GRAY2BGR)
        circles = cv2.HoughCircles(roi_gray_eye,cv2.HOUGH_GRADIENT,1,20,
                            param1=50,param2=30,minRadius=0,maxRadius=0)
        if circles is None:
            print 'Nothing'
        else:
            circles = np.uint16(np.around(circles))
            circ_count = circles.size / 3
            print "%d circles found" % (circ_count)
            for i in circles[0,:]:
                # draw the outer circle
                cv2.circle(roi_gray_eye,(i[0],i[1]),i[2],(0,255,0),1)
                # draw the center of the circle
                cv2.circle(roi_gray_eye,(i[0],i[1]),2,(0,0,255),1)
                break
        #cv2.imshow('gray2',roi_gray_eye),plt.show()
        #cv2.waitKey(0) 
        plt.imshow(roi_gray_eye,'gray'),plt.show()
        cv2.waitKey(0)    
#cv2.imshow("Keypoints", roi_color_eye)
            
#cv2.namedWindow('image', cv2.WINDOW_NORMAL)
#cv2.imshow('image',img)
#plt.imshow(img,'gray'),plt.show()
cv2.waitKey(0)
cv2.destroyAllWindows()