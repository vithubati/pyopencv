import numpy as np
import cv2
from matplotlib import pyplot as plt
from Queue import Empty
face_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_frontalface_alt.xml')
eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye_tree_eyeglasses.xml')
cuda_eye_cascade = cv2.CascadeClassifier('C:/opencv-master/data/haarcascades_cuda/haarcascade_smile.xml')
frame = cv2.imread('../res/face4.PNG')

gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
faces = face_cascade.detectMultiScale(gray, 1.3, 5)  # returns Rect(x,y,w,h)
if len(faces) > 0:
    print 'face len', len(faces)             
    for (x, y, w, h) in faces: 
        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
        roi_gray = gray[y:y + h, x:x + w]  # gray face ROI
        roi_color = frame[y:y + h, x:x + w]  # color Face ROI
        eyes = eye_cascade.detectMultiScale(roi_gray)
        if len(eyes) > 1:                    
            print 'len', len(eyes) 
            for (ex, ey, ew, eh) in eyes:  
                roi_color_eye = roi_color[ey + 10:ey + eh - 5, ex + 5:ex + ew - 5]
                hsv = cv2.cvtColor(roi_color_eye, cv2.COLOR_BGR2HSV)
                # hsv = hue, sat, value
                sensitivity = 145
                lower_white = np.array([0, 0, 255 - sensitivity])
                upper_white = np.array([255, sensitivity, 255])
                mask = cv2.inRange(hsv, lower_white, upper_white)
                res = cv2.bitwise_and(roi_color_eye, roi_color_eye, mask=mask)
                #tmp = cv2.add(roi_color, res)
                if len(eyes) == 2: 
                    for (ex, ey, ew, eh) in eyes:
                        cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)
                else:
                    print 'No eyes'
                cv2.imshow('sssssssssssssssssssssss', res)
                plt.imshow(cv2.cvtColor(roi_color_eye, cv2.COLOR_BGR2RGB))
                plt.show()
                plt.imshow(cv2.cvtColor(res, cv2.COLOR_BGR2RGB))
                plt.show()
                cv2.waitKey(0)
        else:
            print 'No eyes'
    
else:
    print 'no face' 
cap.release()               
cv2.destroyAllWindows()           
# cv2.imshow('vithu', img)
# cv2.waitKey(0)

