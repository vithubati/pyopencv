import numpy as np
import cv2
from matplotlib import pyplot as plt

face_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_frontalface_alt.xml')
# haarcascade_eye.xml haarcascade_eye_tree_eyeglasses
eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye_tree_eyeglasses.xml')
fileName = '../res/MyMovie.mp4'
fileName2 = '../res/MyMovie.mp4'
cap = cv2.VideoCapture(fileName)
# Setup SimpleBlobDetector parameters.
params = cv2.SimpleBlobDetector_Params()
# Change thresholds
params.minThreshold = 10
params.maxThreshold = 200
# Filter by Area.
params.filterByArea = True
params.minArea = 10
while(1):
    if cap.isOpened():
        # Take each frame
        captured, frame = cap.read()
        
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        if captured:
            faces = face_cascade.detectMultiScale(gray, 1.3, 5) # returns Rect(x,y,w,h)
            for (x,y,w,h) in faces: 
                frame = cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
                roi_gray = gray[y:y+h, x:x+w]
                roi_color = frame[y:y+h, x:x+w]
                eyes = eye_cascade.detectMultiScale(roi_gray)
                for (ex,ey,ew,eh) in eyes:
                    cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),1)
                    #cv2.rectangle(roi_color,(ex,ey+10),(ex+ew,ey+eh-10),(0,255,0),2)
                    ver = (cv2.__version__).split('.')
                    if int(ver[0]) < 3 :
                        detector = cv2.SimpleBlobDetector_Params(params)
                    else : 
                        detector = cv2.SimpleBlobDetector_create(params)
                    roi_gray_eye = roi_gray[ey:ey+eh, ex:ex+ew]
                    roi_color_eye = roi_color[ey:ey+eh, ex:ex+ew]   
                    #cv2.imshow('g', roi_gray_eye)
                    # Detect blobs.
                    keypoints = detector.detect(roi_gray_eye)
                    for sp in keypoints:
                        color = (0, 0, 255)
                        x, y = sp.pt
                        cv2.circle(roi_color_eye, (int(x), int(y)), 5, color)
            cv2.imshow('img',frame)
            k = cv2.waitKey(5) & 0xFF
            if k == 27:
                break
        else:
            print 'Cannot read frame from the video' 
            break
    else:
        print 'Cannot open the video'        
        break
cap.release()        
cv2.destroyAllWindows()