import cv2
import numpy as np
import imutils
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans
import utils
import KMeans2 as km
from matplotlib import pyplot as plt
from numpy.ma.testutils import approx
from numpy.ma.testutils import approx
face_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_frontalface_alt.xml')
eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye_tree_eyeglasses.xml')

fileName = '../res/MyMovie.mp4'
fileName2 = '../res/IMG_4093.MOV'
fileName3 = '../res/MyMovie11.mp4'
cap = cv2.VideoCapture(fileName3)

while 1:
    cv2.waitKey(1)
    if cap.isOpened():
        #print 'captured'
        # Take each frame
        captured, frame = cap.read()
        if captured:
            # grab the dimensions of the image and calculate the center of the image
            #(h, w) = frame.shape[:2]
            #center = (w / 2, h / 2)
             
            # rotate the image by 180 degrees
            #M = cv2.getRotationMatrix2D(center, 270, 0.5)
            #frame = cv2.warpAffine(frame, M, (w, h))
            gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            faces = face_cascade.detectMultiScale(gray, 1.3, 5)  # returns Rect(x,y,w,h)
            if len(faces) >0:
                #print 'face len', len(faces)
                for (x, y, w, h) in faces: 
                    frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
                    roi_gray = gray[y:y + h, x:x + w]  # gray face ROI
                    roi_color = frame[y:y + h, x:x + w]  # color Face ROI
                    #print roi_color.shape
                    totRows, totColumns = roi_color.shape[:2]
                    
                    cX = totColumns/3
                    rY = totRows/3
                    #print totRows, rY
                    #print totColumns, cX
                    gray_mouth_roi = roi_gray[rY*2:totRows, cX:cX*2]
                    color_mouth_roi = roi_color[rY*2:totRows, cX:cX*2]
                    #print color_mouth_roi.shape
                    #print "X", x, "Y", y, "W", w, "H", h 
                    
                    blurred = cv2.GaussianBlur(gray_mouth_roi, (5, 5), 0)
                    thresh =  cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                            cv2.THRESH_BINARY_INV,245,80)
                    
                    image = cv2.cvtColor(thresh, cv2.COLOR_GRAY2RGB)
                    # reshape the image to be a list of pixels
                    #print image.shape
                    image = image.reshape((image.shape[0] * image.shape[1], 3))
                    #print image
                    #print 'img2', len(image)
                    #print image[1700:1900]
                    # cluster the pixel intensities
#                     kMeanclt = KMeans(n_clusters = 2)
#                     kMeanclt.fit(image)
#                     
#                     centorids = kMeanclt.cluster_centers_
#                     labels = kMeanclt.labels_
#                     #print len(labels)
#                     #print kMeanclt.
#                     colors = ["g.", "r."]
#                     #print "Unique", np.unique(image)
#                     #print image
#                     #for i in range(len(image)):
#                         #print ("Colour Values:", image[i], "Labels:", labels[i])
#                         #plt.plot(image[i][0], image[0][1], colors[labels[i]], markersize=10)
#                     
#                     #plt.scatter(centorids[:,0], centorids[:,1], marker="x", s=150, linewidths=5, zorder= 10)
#                         
#                     
#                     # build a histogram of clusters and then create a figure
#                     # representing the number of pixels labeled to each color
#                     hist = utils.centroid_histogram(kMeanclt)
                    
                    p = []
                    num_clusters = 2
    
                    # When do we say the optimization has 'converged' and stop updating clusters
                    opt_cutoff = 0.5
                    #print "Pois",points
                    for i in image:
                        p.append(km.Point(i))
                    
                    clusters = km.kmeans(p, num_clusters, opt_cutoff)
                    #print "Clusters", len(clusters)
                     # Print our clusters
                    labels0 =0 
                    labels1 =0
                    for i,c in enumerate(clusters):
                        for p in c.points:
                            #print " Cluster: ", i, "\t Point :", p
                            if i == 1:
                                labels1 = labels1+1
                            else:
                                labels0 = labels0 +1
                    #print "Label 0: ", labels0
                    #print "Label 1: ", labels1  
                    if labels0 > 80 and labels1 >80:
                        print "yawn Detected"     
                    roi_color[rY*2:totRows, cX:cX*2] = color_mouth_roi    
                    cv2.imshow('dd',frame)
                    if cv2.waitKey(5) & 0xFF == ord('q'):
                        break                    
            else:
                print 'no face'
        else:
            print 'no frame captured'
            break
                    
#cv2.imshow('image',img)
#cv2.waitKey(0)
cap.release()
cv2.destroyAllWindows()
