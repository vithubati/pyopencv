#############################################################################
# Full Imports

import sys
import math
import random
import subprocess
import cv2
import numpy as np
"""
This is a pure Python implementation of the K-Means Clustering algorithmn. The
original can be found here:
http://pandoricweb.tumblr.com/post/8646701677/python-implementation-of-the-k-means-clustering
I have refactored the code and added comments to aid in readability.
After reading through this code you should understand clearly how K-means works.
If not, feel free to email me with questions and suggestions. (iandanforth at
gmail)
This script specifically avoids using numpy or other more obscure libraries. It
is meant to be *clear* not fast.
I have also added integration with the plot.ly plotting service. If you put in
your (free) plot.ly credentials below, it will automatically plot the discovered
clusters and their centroids.
To use plotly integration you will need to:
1. Get a username/key from www.plot.ly/api and enter them below
2. Install the plotly module: pip install plotly
"""

PLOTLY_USERNAME = None
PLOTLY_KEY = None

if PLOTLY_USERNAME:
    from plotly import plotly
vn = 3
def main():
    
    # How many points are in our dataset?
    num_points = 10
    
    # For each of those points how many dimensions do they have?
    dimensions = 3
    
    # Bounds for the values of those points in each dimension
    lower = 0
    upper = 200
    
    # The K in k-means. How many clusters do we assume exist?
    num_clusters = 2
    
    # When do we say the optimization has 'converged' and stop updating clusters
    opt_cutoff = 0.5
    
    # Generate some points
    #points = [makeRandomPoint(dimensions, lower, upper) for i in xrange(num_points)]
    #points = [[0, 0, 0], [255, 255, 255], [255, 255, 255], 
     #         [255, 255, 255], [255, 255, 255], [0, 0, 0], 
      #        [255, 255, 255], [255, 255, 255], [0, 0, 0], [255, 255, 255]]
    imgPoints = getImgPixels().tolist()
    p = []
    #print "Pois",points
    for i in imgPoints:
        print "imgpix", i
        print "Points",Point(i)
        p.append(Point(i))
    
#     #print"P", p
#     #print "Pointssss", points
#     #print "imgPoints", imgPoints[0:50]
#     # Cluster those data!
#     clusters = kmeans(p, num_clusters, opt_cutoff)
#     #clusters = kmeans(p, num_clusters, opt_cutoff)
#     print "Clusters", len(clusters)
#      # Print our clusters
#     labels0 =0 
#     labels1 =0
#     for i,c in enumerate(clusters):
#         for p in c.points:
#             print " Cluster: ", i, "\t Point :", p
#             if i == 1:
#                 labels1 = labels1+1
#             else:
#                 labels0 = labels0 +1
#     print "Label 0: ", labels0
#     print "Label 1: ", labels1    
#             
     #Display clusters using plotly for 2d data
     #This uses the 'open' command on a URL and may only work on OSX.
#      if dimensions == 2 and PLOTLY_USERNAME:
#          print "Plotting points, launching browser ..."
#          plotClusters(clusters)
def makeRandomPoint(n, lower, upper):
    '''
    Returns a Point object with n dimensions and values between lower and
    upper in each of those dimensions
    '''
    s =[random.uniform(lower, upper) for i in range(n)]
    #print"s", s
    p = Point(s)
    #print "p", p
    return p

class Point:
    '''
    An point in n dimensional space
    '''
    def __init__(self, coords):
        '''
        coords - A list of values, one per dimension
        '''
        #print "Coords", coords
        self.coords = coords
        self.n = len(coords)
       # print "N", self.n
    def __repr__(self):
        return str(self.coords)

class Cluster:
    '''
    A set of points and their centroid
    '''
    
    def __init__(self, points):
        '''
        points - A list of point objects
        '''
        
        if len(points) == 0: raise Exception("ILLEGAL: empty cluster")
        # The points that belong to this cluster
        self.points = points
        #print "points", points[0]
        #print "points n", points[0]
        # The dimensionality of the points in this cluster
        self.n = points[0].n
        #print "self n ", self.n
        # Assert that all points are of the same dimensionality
        for p in points:
            #print "p", p
            if p.n != self.n: raise Exception("ILLEGAL: wrong dimensions")
            
        # Set up the initial centroid (this is usually based off one point)
        self.centroid = self.calculateCentroid()
        
    def __repr__(self):
        '''
        String representation of this object
        '''
        return str(self.points)
    
    def update(self, points):
        '''
        Returns the distance between the previous centroid and the new after
        recalculating and storing the new centroid.
        '''
        old_centroid = self.centroid
        self.points = points
        self.centroid = self.calculateCentroid()
        shift = getDistance(old_centroid, self.centroid) 
        return shift
    
    def calculateCentroid(self):
        '''
        Finds a virtual center point for a group of n-dimensional points
        '''
        numPoints = len(self.points)
        # Get a list of all coordinates in this cluster
        coords = [p.coords for p in self.points]
        # Reformat that so all x's are together, all y'z etc.
        unzipped = zip(*coords)
        # Calculate the mean for each dimension
        centroid_coords = [math.fsum(dList)/numPoints for dList in unzipped]
        
        return Point(centroid_coords)

def kmeans(points, k, cutoff):
    
    # Pick out k random points to use as our initial centroids
    initial = random.sample(points, k)
    #print "init", initial
    # Create k clusters using those centroids
    clusters = [Cluster([p]) for p in initial]
    
    # Loop through the dataset until the clusters stabilize
    loopCounter = 0
    while True:
        # Create a list of lists to hold the points in each cluster
        lists = [ [] for c in clusters]
        clusterCount = len(clusters)
        
        # Start counting loops
        loopCounter += 1
        # For every point in the dataset ...
        for p in points:
            # Get the distance between that point and the centroid of the first
            # cluster.
            smallest_distance = getDistance(p, clusters[0].centroid)
        
            # Set the cluster this point belongs to
            clusterIndex = 0
        
            # For the remainder of the clusters ...
            for i in range(clusterCount - 1):
                # calculate the distance of that point to each other cluster's
                # centroid.
                distance = getDistance(p, clusters[i+1].centroid)
                # If it's closer to that cluster's centroid update what we
                # think the smallest distance is, and set the point to belong
                # to that cluster
                if distance < smallest_distance:
                    smallest_distance = distance
                    clusterIndex = i+1
            lists[clusterIndex].append(p)
        
        # Set our biggest_shift to zero for this iteration
        biggest_shift = 0.0
        
        # As many times as there are clusters ...
        for i in range(clusterCount):
            # Calculate how far the centroid moved in this iteration
            shift = clusters[i].update(lists[i])
            # Keep track of the largest move from all cluster centroid updates
            biggest_shift = max(biggest_shift, shift)
        
        # If the centroids have stopped moving much, say we're done!
        if biggest_shift < cutoff:
            #print "Converged after %s iterations" % loopCounter
            break
    return clusters

def getDistance(a, b):
    '''
    Euclidean distance between two n-dimensional points.
    Note: This can be very slow and does not scale well
    '''
    if a.n != b.n:
        #raise Exception("ILLEGAL: non comparable points")
        return 0.0
    ret = reduce(lambda x,y: x + pow((a.coords[y]-b.coords[y]), 2),range(a.n),0.0)
    return math.sqrt(ret)

def plotClusters(data):
    '''
    Use the plotly API to plot data from clusters.
    
    Gets a plot URL from plotly and then uses subprocess to 'open' that URL
    from the command line. This should open your default web browser.
    '''
    
    # List of symbols each cluster will be displayed using    
    symbols = ['circle', 'cross', 'triangle-up', 'square']

    # Convert data into plotly format.
    traceList = []
    for i, c in enumerate(data):
        data = []
        for p in c.points:
            data.append(p.coords)
        # Data
        trace = {}
        trace['x'], trace['y'] = zip(*data)
        trace['marker'] = {}
        trace['marker']['symbol'] = symbols[i]
        trace['name'] = "Cluster " + str(i)
        traceList.append(trace)
        # Centroid (A trace of length 1)
        centroid = {}
        centroid['x'] = [c.centroid.coords[0]]
        centroid['y'] = [c.centroid.coords[1]]
        centroid['marker'] = {}
        centroid['marker']['symbol'] = symbols[i]
        centroid['marker']['color'] = 'rgb(200,10,10)'
        centroid['name'] = "Centroid " + str(i)
        traceList.append(centroid)
    
    # Log in to plotly
    py = plotly(username=PLOTLY_USERNAME, key=PLOTLY_KEY)

    # Style the chart
    datastyle = {'mode':'markers',
             'type':'scatter',
             'marker':{'line':{'width':0},
                       'size':12,
                       'opacity':0.6,
                       'color':'rgb(74, 134, 232)'}}
    
    resp = py.plot(*traceList, style = datastyle)
    
    # Display that plot in a browser
    cmd = "open " + resp['url']
    subprocess.call(cmd, shell=True)

def getImgPixels():
    face_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_frontalface_alt.xml')
    eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye_tree_eyeglasses.xml')
    
    img1 = '../res/face.PNG'
    img2 ='../res/yawn1.png'
    img = cv2.imread(img2)
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)  # returns Rect(x,y,w,h)
    for (x, y, w, h) in faces: 
        frame = cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
        roi_gray = gray[y:y + h, x:x + w]  # gray face ROI
        roi_color = img[y:y + h, x:x + w]  # color Face ROI
        #print roi_color.shape
        totRows, totColumns = roi_color.shape[:2]
        
        cX = totColumns/3
        rY = totRows/3
        #print totRows, rY
        #print totColumns, cX
        gray_mouth_roi = roi_gray[rY*2:totRows, cX:cX*2]
        color_mouth_roi = roi_color[rY*2:totRows, cX:cX*2]
        #print color_mouth_roi.shape
        #print "X", x, "Y", y, "W", w, "H", h 
        
        #cv2.imshow('Mouth',gray_mouth_roi)
        #resized = imutils.resize(mouth_roi, width=300)
        #ratio = int(mouth_roi.shape[0] / float(resized.shape[0]))
        # convert the resized image to grayscale, blur it slightly,
        # and threshold it
        #gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray_mouth_roi, (5, 5), 0)
        thresh =  cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                cv2.THRESH_BINARY,245,80)
        
        #thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]
        #cv2.imshow('thresh', thresh)
        
        #print 'img1', image.shape
        image = cv2.cvtColor(thresh, cv2.COLOR_GRAY2RGB)
        # reshape the image to be a list of pixels
        #print image.shape
        image = image.reshape((image.shape[0] * image.shape[1], 3))
        
    return image
    
if __name__ == "__main__": 
    main()