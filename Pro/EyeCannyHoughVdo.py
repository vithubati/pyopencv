import numpy as np
import cv2
from matplotlib import pyplot as plt
from Queue import Empty
face_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_frontalface_alt.xml')
eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye_tree_eyeglasses.xml')

fileName = '../res/MyMovie.mp4'
fileName2 = '../res/MyMovie9.mp4'
s = 1
t = 1
cap = cv2.VideoCapture(fileName2)
print 'System Start'

while(1):
    cv2.waitKey(1)
    
    if cap.isOpened():
        print 'captured'
        # Take each frame
        captured, frame = cap.read()
        
        if captured:
            face_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_frontalface_alt.xml')
            eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye_tree_eyeglasses.xml')
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            # returns Rect(x,y,w,h) of Face Region
            faces = face_cascade.detectMultiScale(gray, 1.3, 5)  
            if len(faces) >0:
                print 'face len', len(faces)             
                for (x, y, w, h) in faces: 
                    frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
                    roi_gray = gray[y:y + h, x:x + w]  # gray face ROI
                    roi_color = frame[y:y + h, x:x + w]  # color Face ROI
                    # returns Rect(x,y,w,h) of Eye Region 
                    eyes = eye_cascade.detectMultiScale(roi_gray)
                    if len(eyes) >1:                    
                        print 'len', len(eyes) 
                        for (ex, ey, ew, eh) in eyes:  
                            print 'eye Found'
                            # cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2) 
                            # cv2.rectangle(roi_color,(ex,ey+10),(ex+ew,ey+eh-10),(0,255,0),2)
                            roi_gray_eye = roi_color[ey:ey + eh, ex:ex + ew]
                            #plt.imshow(cv2.cvtColor(roi_gray_eye, cv2.COLOR_BGR2RGB))
                            #plt.show()
                            roi_gray_eye = cv2.cvtColor(roi_gray_eye, cv2.COLOR_BGR2GRAY)  # gray Eye ROI
                             
                            #color_eye_roi = roi_color[ey:ey + eh, ex:ex + ew]
                            #cv2.imshow('image3', color_eye_roi)  # color face ROI
                            #blur = cv2.GaussianBlur(roi_gray_eye,(5,5),0) 
                            #cv2.waitKey(0)  
                            #frame = cv2.medianBlur(blur,5)
                            
                            #ret, thresh1 = cv2.threshold(roi_gray_eye, 127, 255, cv2.THRESH_BINARY)
                            #gues = cv2.adaptiveThreshold()
                            # bluring the moice which will give good output 
                            #roi_gray_eye = cv2.cvtColor(roi_gray_eye, cv2.COLOR_BGR2GRAY)
                            #blur = cv2.GaussianBlur(roi_color_eye, (5, 5), 0)
                            #edges = cv2.Canny(blur, 100, 200)
                             
                            #cv2.imshow('edgee', edges);
                            #cv2.waitKey(0)
                            # drawing circle on the circle edge
                            
                            #color_eye_roi = cv2.cvtColor(~roi_gray_eye, cv2.COLOR_BGR2GRAY)
                            #cv2.imshow('vithu6', color_eye_roi)
                            #cv2.waitKey(0)
                            
                            roi_gray_eye = cv2.equalizeHist(~roi_gray_eye);
                            #cv2.imshow('vithu7', color_eye_roi)
                            #plt.imshow(roi_gray_eye, cmap='gray')
                            #plt.show() 
                            #cv2.waitKey(0)
                            
                            #cv2.cvtColor(color_eye_roi, cv2.COLOR_GRAY2BGR)
                                    
                            circles = cv2.HoughCircles(roi_gray_eye, cv2.HOUGH_GRADIENT, 1, 300,
                                                        param1=50, param2=30, minRadius=0, maxRadius=0)                      
                            if circles is not None:             
                                circles = np.uint16(np.around(circles))
                                for i in circles[0,:]:
                                    print 'Detected iris', i
                                    print 'Number of Detected Iris', t
                                    t=t+1 
                                    # draw the outer circle
                                    #cv2.circle(roi_gray_eye, (i[0], i[1]), i[2], (0, 255, 0),1)
                                    # draw the center of the circle
                                    cv2.circle(roi_gray_eye, (i[0], i[1]), 2, (0, 0, 255),1)                               
                                    #cv2.imshow('eyes', cv2.cvtColor(color_eye_roi,cv2.COLOR_GRAY2BGR))
                                    #cv2.waitKey(0)
                                    #plt.subplot(121), plt.imshow(roi_gray_eye, cmap='gray')
                                    #plt.title('Found circle Image'), plt.xticks([]), plt.yticks([])
                                    #plt.subplot(122), plt.imshow(frame, cmap='gray')
                                    #plt.title('original Image'), plt.xticks([]), plt.yticks([])            
                                    #plt.show() 
                                    break
                            else:
                                print 'No iris found'  
                    else:
                        print 'No eyes'
            else:
                print 'no face' 
        else:
            print 'no frame captured'
            break
        #=======================================================================
        # plt.subplot(121), plt.imshow(color_eye_roi, cmap='gray')
        # plt.title('Found circle Image'), plt.xticks([]), plt.yticks([])
        # plt.subplot(122), plt.imshow(img, cmap='gray')
        # plt.title('original Image'), plt.xticks([]), plt.yticks([])            
        # plt.show() 
        #=======================================================================
        #cv2.imshow('vithu', color_eye_roi)
        #if cv2.waitKey(5) & 0xFF == ord('q'):
            #break
        print 'Number of Frame', s
        s=s+1
    else:
        print 'no video opened'   
cap.release()             
#cv2.imshow('vithu', img)
#cv2.waitKey(0)
   
