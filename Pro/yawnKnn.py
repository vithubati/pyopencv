import cv2
import numpy as np
import imutils
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import argparse
import utils
from matplotlib import pyplot as plt
from numpy.ma.testutils import approx

face_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_frontalface_alt.xml')
eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye_tree_eyeglasses.xml')

img1 = '../res/face.PNG'
img2 ='../res/yawn1.png'
img = cv2.imread(img2)
gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

faces = face_cascade.detectMultiScale(gray, 1.3, 5)  # returns Rect(x,y,w,h)
for (x, y, w, h) in faces: 
    frame = cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
    roi_gray = gray[y:y + h, x:x + w]  # gray face ROI
    roi_color = img[y:y + h, x:x + w]  # color Face ROI
    #print roi_color.shape
    totRows, totColumns = roi_color.shape[:2]
    
    cX = totColumns/3
    rY = totRows/3
    #print totRows, rY
    #print totColumns, cX
    gray_mouth_roi = roi_gray[rY*2:totRows, cX:cX*2]
    color_mouth_roi = roi_color[rY*2:totRows, cX:cX*2]
    #print color_mouth_roi.shape
    #print "X", x, "Y", y, "W", w, "H", h 
    plt.imshow(cv2.cvtColor(color_mouth_roi, cv2.COLOR_BGR2RGB))
    plt.show()
    cv2.waitKey(0)
    #cv2.imshow('Mouth',gray_mouth_roi)
    #resized = imutils.resize(mouth_roi, width=300)
    #ratio = int(mouth_roi.shape[0] / float(resized.shape[0]))
    # convert the resized image to grayscale, blur it slightly,
    # and threshold it
    #gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray_mouth_roi, (5, 5), 0)
    thresh =  cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv2.THRESH_BINARY,245,80)
    
    #thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]
    #cv2.imshow('thresh', thresh)
    
    #print 'img1', image.shape
    image = cv2.cvtColor(thresh, cv2.COLOR_GRAY2RGB)
    # reshape the image to be a list of pixels
    #print image.shape
    image = image.reshape((image.shape[0] * image.shape[1], 3))
    #print image
    #print 'img2', len(image)
    #print image[1700:1900]
    # cluster the pixel intensities
    print "image", image
    kMeanclt = KMeans(n_clusters = 2)
    kMeanclt.fit(image)
    
    centorids = kMeanclt.cluster_centers_
    labels = kMeanclt.labels_
    print "labels", np.unique(labels)
    print "centroids", centorids
    colors = ["g.", "r."]
    print "Unique", np.unique(image)
    print image
#     for i in range(len(image)):
#         #print ("Colour Values:", image[i], "Labels:", labels[i])
#         plt.plot(image[i][0], image[0][1], colors[labels[i]], markersize=10)
#     
#     plt.scatter(centorids[:,0], centorids[:,1], marker="x", s=150, linewidths=5, zorder= 10)
#         
#     
    # build a histogram of clusters and then create a figure
    # representing the number of pixels labeled to each color
    hist = utils.centroid_histogram(kMeanclt)
#     bar = utils.plot_colors(hist, kMeanclt.cluster_centers_)
    
    
    # show our image
#     plt.figure()
#     plt.axis("off")
#     plt.imshow(bar)
#     plt.show()
    
    #tarr = np.asarray(thresh)
    #print 'Thresh', thresh[30]
    #cv2.imshow('ssas',thresh[30,40])
    
    
    #roi_color[rY*2:totRows, cX:cX*2] = color_mouth_roi    
    #cv2.imshow('dd',thresh)
    #cv2.waitKey(0)
    
#cv2.imshow('image',img)
#cv2.waitKey(0)
#cv2.destroyAllWindows()

# 
# 
# blur = cv2.GaussianBlur(img,(5,5),0)
# edges = cv2.Canny(blur,100,100)
# 
# 
# 
# 
# 
# plt.subplot(121),plt.imshow(img,cmap = 'gray')
# plt.title('Original Image'), plt.xticks([]), plt.yticks([])
# plt.subplot(122),plt.imshow(edges,cmap = 'gray')
# plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
# 
# plt.show()