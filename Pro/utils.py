# import the necessary packages
import numpy as np
import cv2
 
def centroid_histogram(clt):
    # grab the number of different clusters and create a histogram
    # based on the number of pixels assigned to each cluster
    #print "CLT",clt
    numLabels = np.arange(0, len(np.unique(clt.labels_)))
    #print "numLabels",numLabels
    (hist, _) = np.histogram(clt.labels_, bins = numLabels)
    #print len(clt.labels_)
    labels =clt.labels_
    labelsList= labels.tolist()
    #print "white Pixels:", labelsList.count(0)
    #print "Black Pixels:", labelsList.count(1)
    if labelsList.count(0) > 80 and labelsList.count(1) > 80:
        print "YAWN DETECTED"
    
    #print (clt.labels_).count(0)
    # normalize the histogram, such that it sums to one
    hist = hist.astype("float")
    hist /= hist.sum()
 
    # return the histogram
    return hist

def plot_colors(hist, centroids):
    # initialize the bar chart representing the relative frequency
    # of each of the colors
    bar = np.zeros((50, 300, 3), dtype = "uint8")
    startX = 0
 
    # loop over the percentage of each cluster and the color of
    # each cluster
    for (percent, color) in zip(hist, centroids):
        # plot the relative percentage of each cluster
        endX = startX + (percent * 300)
        cv2.rectangle(bar, (int(startX), 0), (int(endX), 50),
            color.astype("uint8").tolist(), -1)
        startX = endX
    
    # return the bar chart
    return bar