import cv2
import numpy as np
import imutils
from matplotlib import pyplot as plt
from numpy.ma.testutils import approx
face_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_frontalface_alt.xml')
eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye_tree_eyeglasses.xml')


fileName = '../res/MyMovie.mp4'
fileName2 = '../res/IMG_4093.MOV'
fileName3 = '../res/MyMovie17.mp4'
cap = cv2.VideoCapture(fileName3)

while 1:
    if cap.isOpened():
        #print 'captured'
        # Take each frame
        captured, frame = cap.read()
        if captured:
            # grab the dimensions of the image and calculate the center of the image
            #(h, w) = frame.shape[:2]
            #center = (w / 2, h / 2)
             
            # rotate the image by 180 degrees
            #M = cv2.getRotationMatrix2D(center, 270, 0.5)
            #frame = cv2.warpAffine(frame, M, (w, h))
            gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            faces = face_cascade.detectMultiScale(gray, 1.3, 5)  # returns Rect(x,y,w,h)
            if len(faces) >0:
                #print 'face len', len(faces)
                for (x, y, w, h) in faces: 
                    frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
                    roi_gray = gray[y:y + h, x:x + w]  # gray face ROI
                    roi_color = frame[y:y + h, x:x + w]  # color Face ROI
                    #print roi_color.shape
                    totRows, totColumns = roi_color.shape[:2]
                    
                    cX = totColumns/3
                    rY = totRows/3
                    #print totRows, rY
                    #print totColumns, cX
                    gray_mouth_roi = roi_gray[rY*2:totRows, cX:cX*2]
                    color_mouth_roi = roi_color[rY*2:totRows, cX:cX*2]
                    #print color_mouth_roi.shape
                    #print "X", x, "Y", y, "W", w, "H", h 
                    
                    cv2.imshow('Mouth',gray_mouth_roi)
                    #resized = imutils.resize(mouth_roi, width=300)
                    #ratio = int(mouth_roi.shape[0] / float(resized.shape[0]))
                    # convert the resized image to grayscale, blur it slightly,
                    # and threshold it
                    #gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
                    blurred = cv2.GaussianBlur(gray_mouth_roi, (5, 5), 0)
                    thresh =  cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                            cv2.THRESH_BINARY_INV,245,30)
                    
                    #thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]
                    
                    # find contours in the thresholded image
                    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                    cnts = cnts[0] if imutils.is_cv2() else cnts[1]
                    for c in cnts:
                        #print c.shape[:2]
                        peri = cv2.arcLength(c, True)
                        #print peri
                        approx = cv2.approxPolyDP(c, 0.04 * peri, True)
                        #print approx.shape
                        #print len(approx)
                     
                        cv2.drawContours(color_mouth_roi, [c], -1, (0, 255, 0), 1)
                        #print color_mouth_roi.shape 
                
                    roi_color[rY*2:totRows, cX:cX*2] = color_mouth_roi    
                    cv2.imshow('dd',frame)
                    if cv2.waitKey(5) & 0xFF == ord('q'):
                        break                    
            else:
                print 'no face'
        else:
            print 'no frame captured'
            break
                    
#cv2.imshow('image',img)
#cv2.waitKey(0)
cap.release()
cv2.destroyAllWindows()

# 
# 
# blur = cv2.GaussianBlur(img,(5,5),0)
# edges = cv2.Canny(blur,100,100)
# 
# 
# 
# 
# 
# plt.subplot(121),plt.imshow(img,cmap = 'gray')
# plt.title('Original Image'), plt.xticks([]), plt.yticks([])
# plt.subplot(122),plt.imshow(edges,cmap = 'gray')
# plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
# 
# plt.show()