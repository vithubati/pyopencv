import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('../res/face3.PNG',0)
ret,thresh1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)


blur = cv2.GaussianBlur(img,(5,5),0)
edges = cv2.Canny(blur,100,200)





plt.subplot(121),plt.imshow(img,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(edges,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

plt.show()