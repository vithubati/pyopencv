import cv2
import numpy as np
from matplotlib import pyplot as plt
 
img = cv2.imread('../res/face3.PNG')
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
cv2.imshow('s2',img)
#cv2.waitKey(0)
eq = cv2.equalizeHist(img);
cv2.imshow('s3',eq)
cv2.waitKey(0)
#res = np.hstack(img, eq)
 
#cv2.imshow('s4',res)
#cv2.waitKey(0)

img = cv2.imread('../res/face3.PNG')
grimg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

kernal = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5,5))
close = cv2.morphologyEx(grimg, cv2.MORPH_CLOSE, kernal)
div = np.float32(grimg)/(close)
res = np.uint8(cv2.normalize(div,div,0,255,cv2.NORM_MINMAX))
plt.imshow(res,'gray'),plt.show()
cv2.waitKey(0) 