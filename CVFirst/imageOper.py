import cv2 
import numpy as np

img = cv2.imread('../src/red.jpg')
px = img[100, 100]
print px

print '\n'
img2 = cv2.imread('../src/food.jpg')
px = img2[100, 100]
print px
# accessing only blue pixel
blue = img2[100, 100, 0]
print blue
# modify the pixel value at 100 100
img[100, 100] = [221, 0, 0]

# for modifying individual pixel
# accessing RED value
print img.item(10, 10, 2)
# modifying RED value
img.itemset((10, 10, 2), 100)
print img.item(10, 10, 2)  # or print img[10,10,2]
# accessing Shape of image (h,w,channels) or (rows,columns,channels) 
print 'Shape ', img.shape
print img2.shape
# Total number of pixels is  (h*w*channels) or (rows*columns*channels)  
print img.size
# Image datatype is obtained by 
print img.dtype

cv2.imshow('Hello',img) 
cv2.waitKey(0) 
cv2.destroyAllWindows()
