import numpy as np
import cv2

cap = cv2.VideoCapture('../res/IMG_4093.MOV')
print cap.isOpened()
while(cap.isOpened()):
    cv2.waitKey(1)
    ret, frame = cap.read()
    
    # grab the dimensions of the image and calculate the center of the image
    (h, w) = frame.shape[:2]
    center = (w / 2, h / 2)
     
    # rotate the image by 180 degrees
    M = cv2.getRotationMatrix2D(center, 270, 0.5)
    rotated = cv2.warpAffine(frame, M, (w, h))

    gray = cv2.cvtColor(rotated, cv2.COLOR_BGR2GRAY)

    cv2.imshow('frame',rotated)
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()