import cv2
import numpy as np
from cv2 import waitKey

# Create a black image
imgs = np.zeros((512,512,3), np.uint8)
# Draw a diagonal blue line with thickness of 5 px
img = cv2.line(imgs,(0,0),(511,511),(255,0,0),5)
img = cv2.rectangle(imgs,(384,0),(510,128),(0,255,0),3)
img = cv2.circle(imgs,(447,63), 63, (0,0,255), -1)
img = cv2.ellipse(imgs,(256,256),(100,50),0,0,180,255,-1)
cv2.imshow('image', imgs)
cv2.waitKey(0)
# wait for ESC key to exit
cv2.destroyAllWindows()
    