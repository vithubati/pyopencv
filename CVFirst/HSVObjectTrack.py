import cv2
import numpy as np
#cap = cv2.VideoCapture(0)
img = cv2.imread('../res/ball.jpg')
img2 = cv2.imread('../res/green2.jpg')
while(1):
    # Take each frame
    frame = img #cap.read()
    frame2 = img2
    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    hsv2 = cv2.cvtColor(frame2, cv2.COLOR_BGR2HSV)
    
    # define range of blue color in HSV  [H-10, 100,100] and [H+10, 255, 255] 
    lower_blue = np.array([110,50,50])
    upper_blue = np.array([130,255,255])
    # define range of green color in HSV
    lower_green = np.array([50, 50, 120])
    upper_green = np.array([70, 255, 255]) 
    
    # Threshold the HSV image to get only blue colors
    Thresholdmask = cv2.inRange(hsv, lower_blue, upper_blue)
    # same as only blue colors 
    Thresholdmask2 = cv2.inRange(hsv2, lower_green, upper_green)

    # Bitwise-AND mask and original image
    res = cv2.bitwise_and(frame,frame, mask= Thresholdmask)
    res2 = cv2.bitwise_and(frame2,frame2, mask= Thresholdmask2)

    cv2.imshow('frame',frame)
    #cv2.imshow('mask',Thresholdmask)
    cv2.imshow('res',res)
    
    cv2.imshow('frame2',frame2)
    #cv2.imshow('mask2',Thresholdmask2)
    cv2.imshow('res2',res2)
    
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()