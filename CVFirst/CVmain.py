import cv2 as cv
import numpy as np
from cv2 import waitKey

img = cv.imread('../res/buterfly.jpg',-1)
#img2 = cv.cvtColor(img, cv.COLOR_BGR2HSV)
#cv.imshow('image1',img2)
img3 = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
cv.imshow('image2',img3)
k = cv.waitKey(0)
if k == 27:         # wait for ESC key to exit
    cv.destroyAllWindows()
elif k == ord('s'): # wait for 's' key to save and exit
    cv.imwrite('../src/buterflygray.png',img3)
    cv.destroyAllWindows()