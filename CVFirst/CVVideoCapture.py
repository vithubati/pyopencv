import numpy as np
import cv2

capture = cv2.VideoCapture('../res/visiontraffic.avi')
if capture.isOpened():
    while(True):
        # Capture frame-by-frame
        ret, frame = capture.read()
        # Our operations on the frame come here
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # Display the resulting frame
        cv2.imshow('frame',gray)
        if cv2.waitKey(5) & 0xFF == ord('q'):
            break
else:
    print 'Failed to open the camera connection...'
        
# When everything done, release the capture
capture.release()
cv2.destroyAllWindows()