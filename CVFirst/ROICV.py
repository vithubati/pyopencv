import cv2 
import numpy as np

img = cv2.imread('../src/red.jpg')
px = img[100, 100]
print px

print 'Shape ', img.shape  #(h,w,channels)
# modify the pixel value at 100 100
img[100, 100] = [221, 0, 0]
# Modifying a square portion = [h(:), w(:)] or [y(:), x(:)] or (n(:), m(:))
img[1:373, 1:661] = [221, 0, 0] #1 pixel gap
img[100:274, 100:562] = [0, 221, 0] #100 pixel gap

cv2.imshow('Hello',img) 
cv2.waitKey(0) 
cv2.destroyAllWindows()