import cv2 as cv
import numpy as np
from cv2 import waitKey

img = cv.imread('../src/food.jpg',0)
cv.imshow('image',img)
k = cv.waitKey(0)
if k == 27:         # wait for ESC key to exit
    cv.destroyAllWindows()
elif k == ord('s'): # wait for 's' key to save and exit
    cv.imwrite('foodgray2.png',img)
    cv.destroyAllWindows()